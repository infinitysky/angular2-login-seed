var getUserPublic = require('./getUserPublic');
var getAllUsersPublic = require('./getAllUsersPublic');

module.exports = {
  getUserPublic: getUserPublic,
  getAllUsersPublic: getAllUsersPublic
}